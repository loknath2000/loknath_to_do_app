let form = document.querySelector("form");
let input = document.querySelector("input");
let ul = document.querySelector("#todo-list");
let deleteBtn;


// function for creating list and appending elements to it

function createLi() {
    const li = document.createElement("li");
    const span = document.createElement("span");
    span.textContent = input.value;
    span.style.padding="1rem"
    const label = document.createElement("label");
    label.textContent = "completed";
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    const editButton = document.createElement("button");
    editButton.textContent = "edit";
    editButton.style.fontSize = "1.5rem";
    editButton.style.margin = "1rem";
    const removeButton = document.createElement("button");
    removeButton.textContent = "remove";
    removeButton.style.fontSize = "1.5rem"


    li.appendChild(span);
    li.appendChild(label);
    label.appendChild(checkbox);
    li.appendChild(editButton);
    li.appendChild(removeButton);

    return li;
}


// function for appending list in to unorder list and setting items to local storage


form.addEventListener("submit", (event) => {
    event.preventDefault();

    const li = createLi();

    if (input.value === "") {
        alert("enter the data")
    } else {
        ul.appendChild(li);

        let array = []
        let localTodo = localStorage.getItem('todo-list')
        if (localTodo != null) {
            array = JSON.parse(localTodo)
        }
        let val = input.value;
        let obj = {
            name: val
        }
        array.push(obj)
        // console.log(JSON.parse(JSON.stringify(array)
        // ));
        localStorage.setItem("todo-list", JSON.stringify(array))
        // console.log("hi");
    }



    form.reset();
});

// getting data adding it in to ui after refreshing the page

let data = JSON.parse(localStorage.getItem("todo-list"))
console.log(data);
for (let obj of data) {
    const li = document.createElement("li");
    const span = document.createElement("span");
    span.textContent = obj.name;
    const label = document.createElement("label");
    label.textContent = "completed";
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    const editButton = document.createElement("button");
    editButton.textContent = "edit";
    editButton.style.fontSize = "1.5rem"
    editButton.style.margin = "1rem";
    const removeButton = document.createElement("button");
    removeButton.textContent = "remove";
    removeButton.style.fontSize = "1.5rem"




    li.appendChild(span);
    li.appendChild(label);
    label.appendChild(checkbox);
    li.appendChild(editButton);
    li.appendChild(removeButton);

    ul.append(li);

}

// function for checked checkbox


ul.addEventListener("change", (event) => {
    const checkbox = event.target;
    const checked = checkbox.checked;
    const li = checkbox.parentNode.parentNode;
    if (checked) {
        li.className = "responded";
    } else {
        li.className = "";
    }
});



// function for editing and saving

let old;
let newText;
ul.addEventListener("click", (event) => {

    if (event.target.tagName === "BUTTON") {
        const button = event.target;

        if (button.textContent === "edit") {
            button.parentNode.firstElementChild.contentEditable = true;
            button.parentNode.firstElementChild.style.background = "white";
            button.parentNode.firstElementChild.style.color = "black";

            old = button.parentNode.firstElementChild.textContent;
            button.textContent = "save";
        } else if (button.textContent === "save") {
            button.parentNode.firstElementChild.contentEditable = false;
            button.parentNode.firstElementChild.style.background = 'none';
            button.parentNode.firstElementChild.style.color = "white";
            button.textContent = "edit";
            newText = button.parentNode.firstElementChild.textContent;
            let data = JSON.parse(localStorage.getItem("todo-list"));
            let updatedText = [];
            for (let obj of data) {
                if (obj["name"] == old) {
                    let updateObj = {
                        "name": obj["name"] = newText
                    }
                    updatedText.push(updateObj)
                } else {
                    updatedText.push({
                        "name": obj["name"]
                    });
                }
            }
            localStorage.setItem("todo-list", JSON.stringify(updatedText));
        }
    }

})


// function for removing


ul.addEventListener("click", (event) => {
    if (event.target.tagName === "BUTTON") {
        const button = event.target;
        const li = button.parentNode;
        const ul = li.parentNode;
        if (button.textContent === "remove") {
            ul.removeChild(li);
            let todos = JSON.parse(localStorage.getItem("todo-list"));
            
            const todoIndex = button.parentNode.firstElementChild.textContent;
            let updatedData = [];
            for(let object of todos){
                if(object.name !== todoIndex){
                    let single = {name: object.name}
                    updatedData.push(single);
                }
            }
            localStorage.setItem("todo-list", JSON.stringify(updatedData));
        }
    }
});